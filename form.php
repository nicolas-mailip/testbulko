<?php

if (!isset($_POST['email']) || !isset($_POST['phone']))
    header('Location: index.php');
if (isset($_POST['nom']))
    setcookie('nom', htmlspecialchars($_POST['nom']), time() + 365 * 24 * 3600, null, null, false, true);
setcookie('mail', htmlspecialchars($_POST['email']), time() + 365 * 24 * 3600, null, null, false, true);
setcookie('phone', htmlspecialchars($_POST['phone']), time() + 365 * 24 * 3600, null, null, false, true);

try {
    $bdd = new PDO('mysql:host=localhost;dbname=bulko;charset=utf8', 'root', '');
} catch (Exception $e) {
    die('Erreur :' . $e->getMessage());
}

$req = $bdd->prepare('INSERT INTO formulaire(nom, email, phone, message) 
VALUE (:nom, :email, :phone, :message)');
$req->execute(array(
    'nom' => $_POST['nom'],
    'email' => $_POST['email'],
    'phone' => $_POST['phone'],
    'message' => $_POST['message']
));

header('Location: index.php');
