<html>
<head>
    <link href="/css/style.css" media="screen, projection" rel="stylesheet" type="text/css"/>
    <link href="/css/mobile.css" media="screen, projection" rel="stylesheet" type="text/css"/>
    <link href='https://fonts.googleapis.com/css?family=Josefin+Sans:300' rel='stylesheet' type='text/css'>
</head>
<body>
<header>
    <a href="http://bulko.-net" class="logo"><img src="/images/logoBulko.png" alt=""></a>
    <a href="" class="social"><img src="/images/socialnetworks.png" alt=""></a>
</header>
<section>
    <div class="background"></div>
    <div class="contact">
        <form action="form.php" method="POST" onsubmit="return checkForm(this)">
            <div class="nom">
                <label for="Nom"></label>
                <input type="text"
                       id="nom" <?php echo((isset($_COOKIE['nom'])) ? ("value = " . $_COOKIE['nom'] . "") : ""); ?>
                       name="nom" placeholder="Nom"/>
            </div>
            <div class="message">
                <label for="message"></label>
                <textarea id="message" name="message" placeholder="Message"></textarea>
            </div>
            <div class="email">
                <label for="Mail"></label>
                <input type="text" id="email"
                       name="email" <?php echo((isset($_COOKIE['mail'])) ? ("value = " . $_COOKIE['mail'] . "") : ""); ?>
                       placeholder="Mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" onblur="checkMail(this)"
                       required/>
            </div>
            <div class="phone">
                <label for="phone"></label>
                <input type="text" id="phone"
                       name="phone" <?php echo((isset($_COOKIE['phone'])) ? ("value = " . $_COOKIE['phone'] . "") : ""); ?>
                       placeholder="T&eacute;l&eacute;phone" onblur="checkPhone(this)" pattern="^0\d{9}" required/>
            </div>
            <div class="send">
                <input type="submit" value="ENVOYER">
            </div>
            <div class="clear"></div>
        </form>
    </div>
    </div>
</section>
</body>
<script src="/js/formvalid.js"></script>
</html>