function checkMail(email) {
    var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[A-Za-z]{2,4}$/;

    if (email.value.length > 0 && !regex.test(email.value))
        return false;
    return true;
}

function checkPhone(phone) {
    var regex = /^0/;

    if (phone.value.length > 0 && (!regex.test(phone.value) || phone.value.length != 10))
        return false;
    return true;
}

function checkForm(form) {
    var phone = checkPhone(form.phone);
    var mail = checkMail(form.email);

    if (phone && mail)
        return true;
    else {
        alert("Veuillez vérifier le contenu de tout les champs.");
        return false;
    }
}
